package com.energy.comenergyana.bean.user;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by misty on 2018/7/23.
 */
@Setter
@Getter
public class Result {

    String result;

    List<OperInfo> oper;

}

package com.energy.comenergyana.bean.user;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by misty on 2018/7/23.
 */
@Getter
@Setter
public class MenuNode {

    private String id;

    private String name;

    private String href="";

    private Boolean active=false;

    private String remark;

    private String subFunctionType;

    private List<MenuNode> subs;

}

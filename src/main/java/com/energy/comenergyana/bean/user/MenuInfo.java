package com.energy.comenergyana.bean.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by misty on 2018/7/23.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MenuInfo {

    private Long menuId;

    private Long parentMenuId;

    private Long rootId;
    private Long menuLevel;
    private String menuName;
    private String reference;
    private Long orderNo;
    private Long isValid;
    private String objLimit;
    private Long objLimitNum;
    private String functionLimit;
    private Long menuState;
    private String menuAliasName;
    private Long subMenuLevel;
    private String reMark;
    private String subFunctionType;

    private String operId;

}

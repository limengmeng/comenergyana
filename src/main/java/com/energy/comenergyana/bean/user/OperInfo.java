package com.energy.comenergyana.bean.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OperInfo {

	private Long operId;

    private String loginName;

    private String operName;

    private String operPassword;

    private String operIp;

    private String operMac;

    private String operEmail;

    private String operTelephone;

    private Long operState;

    private String orgId;

    private String orgName;

    private String  operRole;

    private Long operIsValid;

    private Long roleId;

    private String roleName;

    private String remarks;

    private Long orderNo;

    private Long isGrant;

    private Long roleIsValid;

    private Long roleState;

    private String roleMenu;

    private String areaId;

    /**
     * 为保存操作记录
     * 这里需要传入一个操作员id
     * 而且与新增的operInfo的operId区分开
     */
    private String operIdu;

    /**
     * 新增时 需要的参数
     */
    private String passDefine;

}

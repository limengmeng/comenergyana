package com.energy.comenergyana.bean.rolemana;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class menuInfo {

    private String id;

    private String title;

    private String name;

    private String href="";

    private Boolean active=false;

    private String remark;

    private String func;

    private List<menuInfo> children;
}

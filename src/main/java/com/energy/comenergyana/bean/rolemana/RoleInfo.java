package com.energy.comenergyana.bean.rolemana;

/*
* create by ean 2018-08-15
* */

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleInfo {

    private String roleId;

    private String roleName;

    private String remarks;

    private String orderNo;

    private String isGrant;

    private String isValid;

    private String roleState;

    private String roleType;

    private String opers;
}

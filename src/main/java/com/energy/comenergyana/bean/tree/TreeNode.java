package com.energy.comenergyana.bean.tree;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by misty on 2018/10/29.
 */
@Getter
@Setter

public class TreeNode {

    String text;

    String id;

    String nodeId;

    String type;

    String icon;

    String obj_rel_type;

    String title;

    List<TreeNode> nodes;

}

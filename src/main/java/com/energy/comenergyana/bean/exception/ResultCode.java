package com.energy.comenergyana.bean.exception;

/**
 * Created by misty on 2018/10/26.
 */
public class ResultCode {

    /**
     * 网络连接异常
     */
    public static int WEAK_NET_WORK = 10101;

    /**
     * 参数错误
     */
    public static int PARAMETER_ERROR = 10102;

    /**
     * sql异常
     */
    public static int SQL_ERROR = 10103;

    /**
     * 空指针异常
     */
    public static int NULL_POINTER = 10104;

}

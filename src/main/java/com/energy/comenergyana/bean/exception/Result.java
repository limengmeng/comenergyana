package com.energy.comenergyana.bean.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by misty on 2018/10/26.
 */
@Getter
@Setter
@AllArgsConstructor
public class Result {

    private Integer mes_code;

    private String msg;

}

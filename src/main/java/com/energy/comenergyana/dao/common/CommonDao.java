package com.energy.comenergyana.dao.common;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created by misty on 2018/10/29.
 */
@Mapper
public interface CommonDao {

    @Insert({
            "insert into web_log (OPER_ID, LOG_TIME, CONTENT, LOG_TYPE) " +
                    " values(#{operId}, SYSDATE(), #{logContent}, #{logType})"
    })
    int saveLog(@Param("operId") String operId, @Param("logContent")String logContent, @Param("logType")int logType);

}

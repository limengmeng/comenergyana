package com.energy.comenergyana.dao;

import com.energy.comenergyana.bean.user.MenuInfo;
import com.energy.comenergyana.bean.user.OperInfo;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;

/**
 * Created by misty on 2018/7/23.
 */
@Mapper
public interface UserMapper {

    @Select({
            "select wo.LOGIN_NAME,wo.OPER_NAME,wo.AREA_ID,wo.OPER_EMAIL,wo.OPER_TELEPHONE,wo.ORG_ID,wo.OPER_ID," +
            " wrle.ROLE_ID, wrle.IS_GRANT from web_operinfo wo " +
            " left join web_role_oper_rel wro on wro.OPER_ID=wo.OPER_ID " +
            " left join web_roleinfo wrle on wrle.ROLE_ID=wro.ROLE_ID and wrle.IS_VALID=1 " +
            " where wo.LOGIN_NAME=#{loginName} and wo.OPER_PASSWORD=#{operPassword} and wo.OPER_STATE=1"
    })
    List<OperInfo> getUserInfoAndRole(@Param("loginName") String loginName, @Param("operPassword") String operPassword);

    @Select({"select distinct wm.*,wr.SUB_FUNCTION_TYPE from web_menuinfo wm  inner join web_role_menu_rel  wr " +
            " on wm.Menu_ID=wr.MENU_ID and wr.ROLE_ID in (#{roleId}) where wm.IS_VALID=1 and wm.MENU_STATE=1"})
    @Results({
            @Result(column = "MENU_ID", property = "menuId"),
            @Result(column = "MENU_NAME", property = "menuName"),
            @Result(column = "MENU_ALIAS_NAME", property = "menuAliasName"),
            @Result(column = "PARENT_MENU_ID", property = "parentMenuId"),
            @Result(column = "ROOT_ID", property = "rootId"),
            @Result(column = "MENU_LEVEL", property = "menuLevel"),
            @Result(column = "ORDER_NO", property = "orderNo"),
            @Result(column = "SUB_MENU_LEVEL", property = "subMenuLevel"),
            @Result(column = "REFERENCE", property = "reference"),
            @Result(column = "REMARK", property = "reMark")
    })
    List<MenuInfo> getMenuInfo(String roleId);

    @Select({"select distinct wm.* from web_menuinfo wm  inner join web_role_menu_rel  wr on " +
            "wm.Menu_ID=wr.MENU_ID  where wm.IS_VALID=1 and wm.MENU_STATE=1"})
    @Results({
            @Result(column = "MENU_ID", property = "menuId"),
            @Result(column = "MENU_NAME", property = "menuName"),
            @Result(column = "MENU_ALIAS_NAME", property = "menuAliasName"),
            @Result(column = "PARENT_MENU_ID", property = "parentMenuId"),
            @Result(column = "ROOT_ID", property = "rootId"),
            @Result(column = "MENU_LEVEL", property = "menuLevel"),
            @Result(column = "ORDER_NO", property = "orderNo"),
            @Result(column = "SUB_MENU_LEVEL", property = "subMenuLevel"),
            @Result(column = "REFERENCE", property = "reference"),
            @Result(column = "REMARK", property = "reMark"),
            @Result(column = "SUB_FUNCTION_TYPE", property = "subFunctionType")

    })
    List<MenuInfo> getMenu();

    @Select({
            "SELECT * from data_ep"
    })
    List<Map<String, Object>> getMaps();

}

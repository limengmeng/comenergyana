package com.energy.comenergyana.dao.sysDao;

import com.energy.comenergyana.bean.rolemana.RoleInfo;
import com.energy.comenergyana.bean.user.MenuInfo;
import com.energy.comenergyana.bean.user.OperInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by misty on 2018/10/29.
 */
@Mapper
public interface SysMapper {

    @Select({
            "select a.PARENT_MENU_ID,a.ROOT_ID,a.MENU_NAME,a.REFERENCE,a.OBJ_LIMIT_NUM,a.MENU_ALIAS_NAME," +
                    " a.REMARK,a.MENU_STATE,(b.num+1) orderNo from (select t.* from web_menuinfo t " +
                    " where t.MENU_ID=#{menuId}) a left join " +
                    " (select max(t.order_no) num from web_menuinfo t where t.parent_menu_id=#{menuId})b on 1=1"
    })
    List<MenuInfo> getMenuData(String menuId);

    @Select({
            "select t.MENU_ID, t.MENU_NAME, t.PARENT_MENU_ID,t.ORDER_NO from web_menuinfo t where t.Menu_State=1 AND T.IS_VALID=1 order by t.MENU_ID,t.ORDER_NO"
    })
    List<MenuInfo> treeMenuGetData();

    @Select({
            "select t.MENU_ID, t.MENU_NAME, t.PARENT_MENU_ID, t.ORDER_NO from web_menuinfo t where t.menu_id=#{id}"
    })
    MenuInfo treeMenuGetNode(@Param("id") String id);

    @Select({
            "select t.menu_id,CONCAT(t.order_No, t.MENU_NAME) menuName,t.order_No from web_menuinfo t where t.parent_menu_id=#{menuId} and t.Menu_State=1 AND T.IS_VALID=1 order by t.order_No"
    })
    List<MenuInfo> getChildMenu(@Param("menuId") String menuId);

    @Insert({
            "insert into web_menuinfo (PARENT_MENU_ID,ROOT_ID,MENU_NAME,REFERENCE,IS_VALID,order_No,OBJ_LIMIT,OBJ_LIMIT_NUM," +
                    " FUNCTION_LIMIT,MENU_STATE,MENU_ALIAS_NAME,REMARK) " +
                    " values(#{parentMenuId}, #{rootId}, #{menuName}, #{reference}, 1, #{orderNo},'0070000000000001', #{objLimitNum}, " +
                    " 0, #{menuState}, #{menuAliasName}, #{reMark})"
    })
    int addMenu(MenuInfo menuInfo);

    @Select({
            "select * from web_menuinfo t where t.parent_menu_id=#{menuId}"
    })
    List<MenuInfo> ifParentId(@Param("menuId") String menuId);

    @Update({
            "update web_menuinfo set MENU_NAME=#{menuName},REFERENCE=#{reference},OBJ_LIMIT_NUM=#{objLimitNum},MENU_ALIAS_NAME=#{menuAliasName} ,REMARK=#{reMark},MENU_STATE=#{menuState} where menu_id=#{menuId}"
    })
    Integer updateMenu(MenuInfo menuInfo);

    @Delete({
            "delete from web_menuinfo where menu_id=#{menuId}"
    })
    Integer deleteMenu(@Param("menuId") String menuId);


    @Update({
            "update web_menuinfo set order_No=#{orderNo} where menu_id=#{menuId}"
    })
    Integer updateMenuOrderId(@Param("orderNo") int orderNo, @Param("menuId") String menuId);

    @Select({
            "select a.oper_id,a.login_name,a.oper_name,a.oper_password,a.OPER_MAC," +
                    " a.oper_state,a.oper_ip,a.oper_email,a.oper_telephone,a.org_id,role.role_id,ro.role_name from web_operinfo a" +
                    " left join web_role_oper_rel role on role.oper_id=a.oper_id" +
                    " left join web_roleinfo ro on role.role_id=ro.role_id"
    })
    List<OperInfo> getOperInfo();

    @Select({
            "select a.ROLE_ID,a.ROLE_NAME,a.ROLE_STATE,a.remarks from web_roleinfo a"
    })
    List<RoleInfo> getRoleInfoB();

    @Select({
            "select a.ROLE_ID,a.ROLE_NAME,a.remarks,a.ORDER_NO,a.IS_GRANT,a.IS_VALID,a.ROLE_STATE from web_roleinfo a"
    })
    List<RoleInfo> getRoleInfo();

    @Select({
            "select a.ROLE_ID from web_role_oper_rel a where a.oper_id=#{operId}"
    })
    List<OperInfo> getRoleOper(@Param("operId") String operId);

    @Select({
            "select wr.menu_ID from web_role_menu_rel wr inner join web_menuinfo wm on wm.MENU_ID=wr.MENU_ID " +
                    " where wm.IS_VALID=1 and wr.ROLE_ID=#{roleId}"
    })
    List<MenuInfo> getRoleMenu(@Param("roleId") String roleId);

    @Select({
            "delete from web_operinfo where oper_id=#{operId};" +
                    "delete from web_role_oper_rel where oper_id=#{operId}"
    })
    Integer delOper(@Param("operId") String operId);

    @Select({
            "select t.oper_id from web_operinfo t where t.is_valid=1 and t.login_name=#{loginName}"
    })
    List<OperInfo> checkNameOfOper(@Param("loginName") String loginName);

    @Insert({
            "insert into web_operinfo (oper_id,area_id,login_name,oper_name,oper_password,is_objlimit," +
                    " is_valid,org_id,OPER_STATE,OPER_IP,OPER_EMAIL,OPER_TELEPHONE,OPER_MAC) values(" +
                    " #{operId},0,#{loginName},#{operName},#{operPassword},0,1,'27000001',#{operState}," +
                    " #{operIp},#{operEmail},#{operTelephone},#{operMac})"
    })
    @Options(useGeneratedKeys = true, keyProperty = "operId")
    Integer addOpers(OperInfo operInfo);

    @Select({
            "select max(oper_id)+1 id from web_operinfo"
    })
    Long getMaxOperId();

    @Insert({
            "insert into web_role_oper_rel (role_oper_id,oper_id,role_id) values(null,#{id}, #{roleId})"
    })
    Integer addOperRole(@Param("id") Long id, @Param("roleId") String roleId);

    @Update({
            "update web_operinfo set login_name=#{loginName},oper_name=#{operName},oper_ip=#{operIp}," +
                    " oper_mac=#{operMac},oper_email=#{operEmail},oper_telephone=#{operTelephone}," +
                    " oper_password=#{operPassword},OPER_STATE=#{operState} where oper_id=#{operId}"
    })
    Integer updateOper(OperInfo operInfo);

    @Delete({
            "delete from web_role_oper_rel where oper_id=#{operId}"
    })
    Integer deleOperRole(@Param("operId") Long operId);

    @Delete({
            "delete from web_operinfo where oper_id=#{operId}"
    })
    Integer deleOper(@Param("operId") Long operId);

    @Select({
            "select a.oper_id from WEB_ROLE_OPER_REL a, WEB_OPERINFO b where a.oper_id=b.oper_id and b.is_valid=1 and a.role_id=#{roleId}"
    })
    List<OperInfo> checkRole(@Param("roleId") Long roleId);

    @Delete({
            "delete from web_roleinfo where role_id=#{roleId}"
    })
    Integer deleRole(@Param("roleId") Long roleId);

    @Delete({
            "delete from web_role_menu_rel where role_id=#{roleId}"
    })
    Integer deleRoleMenu(@Param("roleId") Long roleId);

    @Select({
            "select t.role_id from web_roleinfo t where t.is_valid=1 and t.role_name=#{roleName}"
    })
    List<OperInfo> checkNameRole(@Param("roleName") String roleName);

//    Long getMaxRoleId();

    @Insert({
            "insert into web_roleinfo (role_name,remarks,is_grant,is_valid, role_state) values(" +
                    "#{roleName}, #{remarks}, 1, 1, #{roleState})"
    })
    @Options(useGeneratedKeys = true, keyProperty = "roleId")
    Integer addRole(OperInfo operInfo);

    @Insert({
            "insert into web_role_menu_rel (MENU_ID, role_id) values(#{menuId}, #{roleId})"
    })
    Integer addRoleMenu(@Param("menuId") String menuId, @Param("roleId") Long roleId);

    @Update({
            "update web_roleinfo set role_name=#{roleName},remarks=#{remarks},role_state=#{roleState} where role_id=#{roleId}"
    })
    Integer updateRole(OperInfo operInfo);



}

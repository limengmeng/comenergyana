package com.energy.comenergyana.exception;

import com.energy.comenergyana.bean.exception.Result;
import com.energy.comenergyana.bean.exception.ResultCode;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;
import java.util.List;

/**
 * @author misty
 * Created by misty on 2018/10/26.
 */
@RestControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(Exception.class)
    public Result handleException(Exception e) {
        e.printStackTrace();
        return new Result(ResultCode.WEAK_NET_WORK, "发生异常，请重试");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result handleIllegalParamException(MethodArgumentNotValidException e) {
        List<ObjectError> errors = e.getBindingResult().getAllErrors();
        String tips = "参数不合法";
        if (errors.size() > 0) {
            tips = errors.get(0).getDefaultMessage();
        }
        Result result = new Result(ResultCode.PARAMETER_ERROR, tips);
        return result;
    }

    @ExceptionHandler(SQLException.class)
    public Result handleSqlException(SQLException e){
        Result result = new Result(ResultCode.SQL_ERROR, "SQL异常");
        return result;
    }

    @ExceptionHandler(NullPointerException.class)
    public Result handleNullPointException(NullPointerException e){
        Result result = new Result(ResultCode.NULL_POINTER, "空指针异常");
        return result;
    }

}

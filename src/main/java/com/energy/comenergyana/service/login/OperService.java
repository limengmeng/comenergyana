package com.energy.comenergyana.service.login;

import com.energy.comenergyana.bean.rolemana.menuInfo;
import com.energy.comenergyana.bean.user.MenuInfo;
import com.energy.comenergyana.bean.user.MenuNode;
import com.energy.comenergyana.bean.user.OperInfo;
import com.energy.comenergyana.dao.UserMapper;
import com.litt.core.security.MD5;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by misty on 2018/7/23.
 */
@Service
@Slf4j
public class OperService {

    @Autowired
    UserMapper userMapper;

    /**
     * 用户登录验证
     * 登录成功返回 用户角色
     * 登录失败 返回验证失败
     * @param loginName
     * @param passWord
     * @return
     */
    public List<OperInfo> getOperLoginInfo(String loginName, String passWord){
        String loginPassword = this.setEncrypt(passWord);
        String operPassword = this.MD5EncodeString(loginPassword);
        log.info("--passWord--" + passWord);
        List<OperInfo> opers = userMapper.getUserInfoAndRole(loginName, operPassword);
        return opers;
    }

    public  String setEncrypt(String str){
        String sn="shxttxhs";
        int[] snNum=new int[str.length()];
        String result="";
        String temp="";

        for(int i=0,j=0;i<str.length();i++,j++){
            if(j==sn.length()){
                j=0;
            }
            snNum[i]=str.charAt(i)^sn.charAt(j);
            if(snNum[i]<10){
                temp="00"+snNum[i];
            }else{
                if(snNum[i]<100){
                    temp="0"+snNum[i];
                }
            }
            result+=temp;
        }
        return result;
    }

    public  String MD5EncodeString(String str)
    {
        MD5 md5 = new MD5();
        String md5Pass = md5.getMD5ofStr(str);
        return md5Pass;
    }

    /**
     * 获取多级菜单
     * @param roleId
     * @return
     */
    public List<MenuNode> getMenubyRole(String roleId){

        List<MenuNode> result = new ArrayList<MenuNode>();

        if(roleId==""||roleId==null){
            return null;
        }
        else{
            List<MenuInfo> menulist = userMapper.getMenuInfo(roleId);
            result=getNodeTree(menulist, 0L);
            for(int i=0;i<result.size();i++){
                MenuNode temp=result.get(i);
                //如果二级菜单不为空
                if(temp.getSubs().size()>0){
                    //获取权限下二级菜单的第一个
                    MenuNode sub1=temp.getSubs().get(0);
                    //如果二级菜单有直接的URL,则直接跳转到二级菜单的URL
                    if(sub1.getHref()!=null&&sub1.getHref()!=""){
                        temp.setHref(sub1.getHref());
                    }else{
                        //如果二级菜单没有URL，则获取三级菜单的第一个
                        if(sub1.getSubs().size()>0){
                            MenuNode sub2=sub1.getSubs().get(0);
                            temp.setHref(sub2.getHref());
                        }
                    }
                }
            }
        }
        return result;
    }

    public List<menuInfo> getMenus(){
        List<menuInfo> result = new ArrayList<menuInfo>();
        List<MenuInfo> menulist = userMapper.getMenu();;
        result=getNodeTree1(menulist, 0L);
        for(int i=0;i<result.size();i++){
            menuInfo temp=result.get(i);
            //如果二级菜单不为空
            if(temp.getChildren().size()>0){
                //获取权限下二级菜单的第一个
                menuInfo sub1=temp.getChildren().get(0);
                //如果二级菜单有直接的URL,则直接跳转到二级菜单的URL
                if(sub1.getHref()!=null&&sub1.getHref()!=""){
                    temp.setHref(sub1.getHref());
                }else{
                    //如果二级菜单没有URL，则获取三级菜单的第一个
                    if(sub1.getChildren().size()>0){
                        menuInfo sub2=sub1.getChildren().get(0);
                        temp.setHref(sub2.getHref());
                    }
                }
            }
        }
        log.info("menu-" + result);
        return result;
    }

    private List<menuInfo> getNodeTree1(List<MenuInfo> menulist, Long rootId){
        List<menuInfo> result=new ArrayList<menuInfo>();
        List<MenuInfo> subs=getSubs(menulist, rootId);
        if(subs.size()==0){
            return result;
        }else{
            for(int i=0;i<subs.size();i++){
                MenuInfo temp=subs.get(i);
                menuInfo node=new menuInfo();
                node.setId(String.valueOf(temp.getMenuId()));
                node.setTitle(temp.getMenuName());

                node.setFunc(temp.getFunctionLimit());
                if(temp.getReference()!=null){
                    node.setHref(temp.getReference());
                }
                node.setRemark(temp.getReMark());
                node.setChildren(getNodeTree1(menulist,temp.getMenuId()));
                result.add(node);
            }
        }
        return result;

    }

    private List<MenuNode> getNodeTree(List<MenuInfo> menulist, Long rootId){
        List<MenuNode> result=new ArrayList<MenuNode>();
        List<MenuInfo> subs=getSubs(menulist, rootId);
        if(subs.size()==0){
            return result;
        }else{
            for(int i=0;i<subs.size();i++){
                MenuInfo temp=subs.get(i);
                MenuNode node=new MenuNode();
                node.setId(String.valueOf(temp.getMenuId()));
                node.setName(temp.getMenuName());
                node.setSubFunctionType(temp.getSubFunctionType());
                if(temp.getReference()!=null){
                    node.setHref(temp.getReference());
                }
                node.setRemark(temp.getReMark());
                node.setSubs(getNodeTree(menulist,temp.getMenuId()));
                result.add(node);
            }
        }
        return result;

    }

    private List<MenuInfo> getSubs(List<MenuInfo> menulist,Long rootId){
        List<MenuInfo> result=new ArrayList<MenuInfo>();
        for(int i=0;i<menulist.size();i++){
            MenuInfo temp=menulist.get(i);
//            System.out.println(temp.getParentMenuId());
            if(String.valueOf(temp.getParentMenuId()).equalsIgnoreCase(String.valueOf(rootId))){
                result.add(temp);
            }
        }
        Collections.sort(result, new Comparator<MenuInfo>() {
            @Override
            public int compare(MenuInfo o1, MenuInfo o2) {
                return o1.getOrderNo().compareTo(o2.getOrderNo());
            }
        });
        return result;
    }

}

package com.energy.comenergyana.service.common;

import com.energy.comenergyana.dao.common.CommonDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by misty on 2018/10/29.
 */
@Service
public class CommonService {

    @Autowired
    CommonDao commonDao;

    public void saveLog(String operId, String logContent, int logType){
        commonDao.saveLog(operId, logContent, logType);
    }

}

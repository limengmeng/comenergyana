package com.energy.comenergyana.service.treeBuilder;

import com.energy.comenergyana.bean.tree.TreeNode;
import com.energy.comenergyana.bean.user.MenuInfo;
import com.energy.comenergyana.dao.sysDao.SysMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by misty on 2018/10/29.
 */
@Service
public class MenuTreeBuilder {

    @Autowired
    SysMapper sysDao;

    private List<TreeNode> treeList;

    private List<String> treeType;

    private List<String> icons;


    private List<TreeNode> getTree(List<MenuInfo> ecList,MenuInfo root){
        List<TreeNode> nodeList=null;
        List<TreeNode> tempList=new ArrayList<TreeNode>();
        String tempid=root.getMenuId().toString();
        for(int i=0;i<ecList.size();i++){
            MenuInfo tempECG=ecList.get(i);
            if(tempECG.getParentMenuId().toString().equalsIgnoreCase(tempid)){
                TreeNode tempNode=new TreeNode();
                tempNode.setId(tempECG.getMenuId().toString());
                tempNode.setText(tempECG.getMenuName());
                tempNode.setNodes(getTree(ecList,tempECG));
                tempList.add(tempNode);
            }
        }
        if(tempList.size()>0){
            nodeList=tempList;
        }
        return nodeList;
    }

    public List<TreeNode> construct_rebuilding(){
        List<TreeNode> treeList = new ArrayList<TreeNode>();
//        treeList.clear();
        List<MenuInfo> ECGList = sysDao.treeMenuGetData();
        for(int i=0;i<ECGList.size();i++){
            MenuInfo temp = ECGList.get(i);
            if(temp.getParentMenuId()==0){
                MenuInfo root = sysDao.treeMenuGetNode(temp.getMenuId().toString());
                TreeNode node = new TreeNode();
                node.setId(root.getMenuId().toString());
                node.setText(root.getMenuName());
                node.setNodes(getTree(ECGList, root));
                treeList.add(node);
            }
        }
        return treeList;
    }

}

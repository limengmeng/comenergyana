package com.energy.comenergyana.service.sys;

import com.energy.comenergyana.bean.rolemana.RoleInfo;
import com.energy.comenergyana.bean.tree.TreeNode;
import com.energy.comenergyana.bean.user.MenuInfo;
import com.energy.comenergyana.bean.user.OperInfo;
import com.energy.comenergyana.dao.sysDao.SysMapper;
import com.energy.comenergyana.service.common.CommonService;
import com.energy.comenergyana.service.login.OperService;
import com.energy.comenergyana.service.treeBuilder.MenuTreeBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by misty on 2018/10/29.
 */
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class SysService {

    @Autowired
    MenuTreeBuilder treeServie;

    @Autowired
    SysMapper sysDao;

    @Autowired
    CommonService commonService;

    @Autowired
    OperService operService;

    public List getMenuData(String type, String menuId){
        if(type.equals("menuInfo")){
            List<MenuInfo> sys = sysDao.getMenuData(menuId);
            return sys;
        }
        else if(type.equals("tree")){
            List<TreeNode> trees = treeServie.construct_rebuilding();
            return trees;
        }
        else if(type.equals("childMenu")){
            List<MenuInfo> sys = sysDao.getChildMenu(menuId);
            return sys;
        }
        return null;
    }

    public String postMenuData(String type, MenuInfo menuInfo){
        String res = "";
        String logContent = "";
        if(type.equals("add")){
            int num = sysDao.addMenu(menuInfo);
            log.info("新增" + num);
            if(num > 0){
                res = "新增成功";
                logContent = "新增名字为" + menuInfo.getMenuName() + "的菜单";
            }
            else {
                res = "新增失败";
            }
        }
        else if(type.equals("del")){
            List<MenuInfo> list = sysDao.ifParentId(menuInfo.getMenuId() + "");
            if(null!=list && list.size()>0){
                res = "菜单为父级菜单，不能删除";
            }
            else {
                int num = sysDao.deleteMenu(menuInfo.getMenuId() + "");
                if(num > 0){
                    res = "删除成功";
                    logContent = "删除名字为" + menuInfo.getMenuName() +"[" + menuInfo.getMenuId() + "]的菜单";
                }
                else {
                    res = "删除失败";
                }
            }
        }
        else if(type.equals("update")){
            int num = sysDao.updateMenu(menuInfo);
            log.info("更新" + num);
            if(num > 0){
                res = "更新成功";
                logContent = "修改了名字为" + menuInfo.getMenuName() +"[" + menuInfo.getMenuId() + "]的菜单";
            }
            else {
                res = "更新失败";
            }
        }
        if(!logContent.equals("")){
            commonService.saveLog(menuInfo.getOperId(), logContent, 2);
        }
        log.info("res--" + res);
        return res;
    }

    public String sortMenu(String operId, String menuName, String serial){
        String sarray[] = serial.split(",");
        int size = sarray.length;
        int res = 0;
        for(int i=0; i<size; i++){
            log.info(sarray[i]);
            res += sysDao.updateMenuOrderId(i+1, sarray[i]);
        }
        String logContent = "修改了菜单" + menuName +"的子菜单顺序";
        commonService.saveLog(operId,  logContent, 2);
        String result = "";
        if(res==size){
            result = "保存成功";
        }
        else {
            result = "保存失败";
        }
        return result;
    }

    public List operRoleInfo(String type, String operId, String roleId){
        if(type.equals("operInfo")){
            List<OperInfo> opers = sysDao.getOperInfo();
            log.info("operInfo-" + opers.toString());
            return opers;
        }
        //没找到用途
        else if(type.equals("roleInfoB")){
            List<RoleInfo> roles = sysDao.getRoleInfoB();
            log.info("roleInfoB-" + roles.toString());
            return roles;
        }
        else if(type.equals("roleInfo")){
            List<RoleInfo> roles = sysDao.getRoleInfo();
            log.info("roleInfo-" + roles.toString());
            return roles;
        }
        //页面operinfo 暂时作废
        else if(type.equals("operRole")){
            List<OperInfo> opers = sysDao.getRoleOper(operId);
            log.info("operRole-" + opers.toString());
            return opers;
        }
        else if(type.equals("roleMenu")){
            List<MenuInfo> list = sysDao.getRoleMenu(roleId);
            log.info("roleMenu-" + list.toString());
            return list;
        }
        return null;
    }

    public String postOperInfo(String type, String stype, OperInfo operInfo){
        String logContent = "";
        String json = "";
        if("deloper".equals(type)){
            Long operId = operInfo.getOperId();
            int op = sysDao.deleOper(operId);
            sysDao.deleOperRole(operId);
            logContent = "删除了名字是" + operInfo.getLoginName() +"["+operId+"]的操作员";
            if(op > 0){
                json = "删除成功";
            }
            else{
                json = "删除失败";
            }
        }
        else if("checknameoper".equals(type)){
            List<OperInfo> opers = sysDao.checkNameOfOper(operInfo.getLoginName());
            if(opers!=null && opers.size()>0){
                json = "1";
            }
            else{
                json = "0";
            }
        }
        else if("saveoper".equals(type)){
            String passDefine = operInfo.getPassDefine();
            String operPassword = operInfo.getOperPassword();
            if(passDefine.equalsIgnoreCase("false")){
                //异或加密
                String loginPassword = operService.setEncrypt(operPassword);
                //加密后的字段
                operPassword = operService.MD5EncodeString(loginPassword);
                operInfo.setOperPassword(operPassword);
            }

            Long id = 0L;
            if("add".equals(stype)){
                log.info("operId-before-" + operInfo.getOperId());
                int num = sysDao.addOpers(operInfo);
                logContent = "添加了名字是" + operInfo.getLoginName() +"["+operInfo.getOperId()+"]的操作员";
                json = "保存成功";
            }
            else{
                //更新
                int num = sysDao.updateOper(operInfo);
                sysDao.deleOperRole(operInfo.getOperId());
                logContent = "修改名字是" + operInfo.getLoginName() +"["+id+"]的操作员";
                json = "更新成功";
            }
            id = operInfo.getOperId();
            log.info("operId-after-" + id);
            String operRole = operInfo.getOperRole();
            String sarray[] = operRole.split(",");
            for(int i=0;i<sarray.length;i++){
                sysDao.addOperRole(id, sarray[i]);
            }
        }
        //角色
        else if("saverole".equals(type)){
            log.info("roleId-before---" + operInfo.getRoleId());
            if(stype.equals("add")){
                int num = sysDao.addRole(operInfo);
                logContent = "添加了名字是" + operInfo.getRoleName() +"的角色";
                json = "新增成功";
            }
            else{
                int num = sysDao.updateRole(operInfo);
                sysDao.deleRoleMenu(operInfo.getRoleId());
                logContent = "修改名字是" + operInfo.getRoleName() +"的角色";
                json = "更新成功";
            }
            Long id = operInfo.getRoleId();
            log.info("roleId--after-" + id);
            String roleMenu = operInfo.getRoleMenu();
            String sarray[] = roleMenu.split(",");
            for(int i=0;i<sarray.length;i++) {
                sysDao.addRoleMenu(sarray[i], id);
            }
        }
        else if("delrole".equals(type)){
            Long roleId = operInfo.getRoleId();
            List<OperInfo> opers = sysDao.checkRole(roleId);
            if(opers!=null && opers.size()>0){
                json = "有从属于该角色的操作员，不可删除!";
            }
            else{
                sysDao.deleRole(roleId);
                sysDao.deleRoleMenu(roleId);
                logContent = "删除了角色:" + operInfo.getRoleName();
                json = "删除成功";
            }
        }
        else if("checknamerole".equals(type)){
            List<OperInfo> list = sysDao.checkNameRole(operInfo.getRoleName());
            if(list!=null && list.size()>0){
                json = "1";
            }
            else {
                json = "0";
            }
        }
        if(logContent.length() > 0){
            commonService.saveLog(operInfo.getOperIdu(), logContent, 2);
        }
        return json;
    }
}

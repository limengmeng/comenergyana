package com.energy.comenergyana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class ComEnergyAnaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComEnergyAnaApplication.class, args);
	}

}

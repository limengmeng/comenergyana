package com.energy.comenergyana.controller.login;

import com.energy.comenergyana.bean.rolemana.menuInfo;
import com.energy.comenergyana.bean.user.MenuNode;
import com.energy.comenergyana.bean.user.OperInfo;
import com.energy.comenergyana.bean.user.Result;
import com.energy.comenergyana.service.login.OperService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by misty on 2018/10/26.
 */
@RestController
@Slf4j
public class LoginController {

    @Autowired
    OperService operService;

    @PostMapping("/Login")
    public Result getOperInfo(String LoginName, String Password){
        log.info(LoginName+"~"+Password);
        List<OperInfo> opers = operService.getOperLoginInfo(LoginName, Password);
        Result result = new Result();
        if(opers.size() > 0){
            result.setResult("success");
            result.setOper(opers);
        }
        else{
            result.setResult("failure");
        }
        return result;
    }

    @GetMapping("/MenuLoading")
    public List<MenuNode> getMeneByRole(String roleId){
        return operService.getMenubyRole(roleId);
    }

    @GetMapping("/Menus")
    public List<menuInfo> getMene(){
        return operService.getMenus();
    }

}

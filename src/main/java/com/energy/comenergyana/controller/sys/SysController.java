package com.energy.comenergyana.controller.sys;

import com.energy.comenergyana.bean.user.MenuInfo;
import com.energy.comenergyana.bean.user.OperInfo;
import com.energy.comenergyana.service.sys.SysService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by misty on 2018/10/29.
 */
@RestController
@Slf4j
public class SysController {

    @Autowired
    SysService sysService;

    @GetMapping("/getMenuData")
    public List getMenuData(String type, String menuId){
        return sysService.getMenuData(type, menuId);
    }

    @PostMapping("/getMenuData/{type}")
    public String saveMenuData(@PathVariable String type, @RequestBody(required = false) MenuInfo menuInfo){
        log.info(menuInfo.getMenuId() + "-" + menuInfo.getMenuName() + "-" + menuInfo.getParentMenuId() + "-" + menuInfo.getOperId());
        String res = sysService.postMenuData(type, menuInfo);
        return res;
    }

    @PostMapping("/sortMenuData")
    public String sortMenu(String operId, String menuName, String serial){
        return sysService.sortMenu(operId, menuName, serial);
    }


    @GetMapping("/getOperData/{type}")
    public List getOperRoleInfo(@PathVariable String type, String operId, String roleId){
        log.info("type-" + type + "-operId-" + operId + "-roleId-" + roleId);
        return sysService.operRoleInfo(type, operId, roleId);
    }

    @PostMapping("/getOperData/{type}/{stype}")
    public String postOperData(@PathVariable String type, @PathVariable String stype, @RequestBody(required = false) OperInfo operInfo){
        log.info(type + "-" + stype + "-" + (operInfo==null));
        return sysService.postOperInfo(type, stype, operInfo);
    }


}
